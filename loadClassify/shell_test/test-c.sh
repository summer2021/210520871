# !/bin/bash



echo start: > test-c-$1.log
date >> test-c-$1.log
for i in {300..1800..300}
do
	echo -$i c$1 is start >> test-c-$1.log
	time redis-benchmark -c $i -n 5000 -r 100 -d 10 -q >> test-c-$1.log
	echo -$i c$1 is end >> test-c-$1.log
	
	
	echo $i c$1 is done
	sleep 1
done
echo "==============================================================================" >> test-c-$1.log
redis-cli flushall
