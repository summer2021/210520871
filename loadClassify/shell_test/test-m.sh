# !/bin/bash

echo start: > test-m-$1.log
date >> test-m-$1.log
for i in {100..2600..500}
do
	echo -$i b$1 is start >> test-m-$1.log
	time redis-benchmark  -n 5000 -r 10 -d $i -q >> test-m-$1.log
	echo -$i b$1 is end >> test-m-$1.log



	echo $i b$1 is done
	sleep 1
done

echo "==============================================================================" >> test-m-$1.log
redis-cli flushall
