## 项目描述

Linux内核下系统内存及进程内存相关信息提取及预测

首先：本题需要提取下列4个指标： 全系统的内存水位变化 按内存段提取进程的内存用量 按进程提取页换入信息 按进程提取巨页缺页错误相关信息 其次：要求从Linux内核中实时提取到相关数据后，经过处理，可以在网页前端展示系统当前的内存情况，并且需要将该功能实现为社区项目LMP的插件，并成功对接到LMP中 最后：需要选择一个合适的预测算法，根据当前内存指标情况，给出下个阶段可能的指标大小，其中指标数据需要存放在LMP的数据存储模块中，并且需要将该模型对接到社区项目LMP中，并能够成功运行。

## 项目详细方案

#### 1. 指标提取

本项目共有四个指标：

##### 1. 全系统的内存水位变化 

每个内存管理区都有一个数组watermark，内核中定义了三个watermark来表示当前系统剩余的空闲内存。



- high 当剩余内存在high以上时，系统认为当前内存使用压力不大。
- low  当剩余内存降低到low时，系统就认为内存已经不足了，会触发kswapd内核线程进行内存回收处理
- min 当剩余内存在min以下时，则系统内存压力非常大。



一般情况下min以下的内存是不会被分配的，min以下的内存默认是保留给特殊用途使用，属于保留的页框，用于原子的内存请求操作。
比如：当我们在中断上下文申请或者在不允许睡眠的地方申请内存时，可以采用标志GFP_ATOMIC来分配内存，此时才会允许我们使用保留在min水位以下的内存。

我们可以使用"cat /proc/zoneinfo"可以查看这三个值的大小（注意这里是以page为单位的）







为了尽量避免出现direct reclaim，我们需要空余内存的大小一直保持在"min"值之上。在网络收发的时候，数据量可能突然增大，需要临时申请大量的内存，这种场景被称为"burst allocation"。此时kswapd回收内存的速度可能赶不上内存分配的速度，造成direct reclaim被触发，影响系统性能。

在内存分配时，只有"low"与"min"之间之间的这段区域才是kswapd的活动空间，低于了"min"会触发direct reclaim，高于了"low"又不会唤醒kswapd，而Linux中默认的"low"与"min"之间的差值确实显得小了点。

> 在Linux内核4.6版本中，诞生了一种新的调节watermark的方式。具体做法是引入一个叫做"watermark_scale_factor"的系数，其默认值为10，对应内存占比0.1%(10/10000)，可通过"/proc/ sys/vm/watermark_scale_factor"设置，最大为1000。当它的值被设定为1000时，意味着"low"与"min"之间的差值，以及"high"与"low"之间的差值都将是内存大小的10%(1000/10000)。



##### 2. 按内存段提取进程的内存用量 

  首先是对内存段的定义，分段存储器模型将系统存储器分为独立的分段组，这些分段由位于分段寄存器中的指针引用。 每个段用于包含特定类型的数据。 一个段用于包含指令代码，另一段用于存储数据元素，第三段用于保留程序堆栈。

  

- **数据段-**由**.data**段和**.bss**表示。 .data节用于声明存储区，在该存储区中为程序存储了数据元素。 声明数据元素后，无法扩展此部分，并且在整个程序中它**保持静态**。在data.bss部分也是一个静态内存部分，其中包含用于稍后在程序中声明的数据的缓冲区。 该缓冲存储器为零。怎么理解呢，就是在.data部分的数据元素中，都是赋值了的，而在.bss中的数据元素都是没有赋值的，就这么简单。
- **代码段-**它由**.text**部分表示。 这在内存中定义了存储指令代码的区域， 这也是一个固定区域。
- **堆栈段**-该段包含传递给程序中的函数和过程的数据值，它并不储存在可执行文件中，它是在程序运行时产生的

  其次，在这里有传统工具pmap可以提取出来进程的内存映射，如下图所示：

  其中，第一列表示虚拟空间地址，第二列表示该项在虚拟空间中占用的大小，第三列表示权限，第四列表示该项名称（anon是佚名的）

  PS：Pmap实际上是一个Sun OS上的命令，Linux仅支持其有限的功能。

  
| 选项                    | 说明                                                         |
| ----------------------- | ------------------------------------------------------------ |
| -x, --extended          | 显示扩展的信息                                               |
| -d, --device            | 显示设备的信息                                               |
| -q, --quiet             | 不显示头或脚注                                               |
| -A, --range low,high    | 只显示指定范围的地址空间，低地址和高地址分别用low和high指定，以逗号分隔 |
| -X                      | 显示比-x更详细的信息。但是会按照文件/proc/PID/smaps改变显示格式（显示的信息是该文件内容的简化） |
| -XX                     | 显示一切内核提供的信息                                       |
| -p, --show-path         | 显示文件项的全路径                                           |
| -c, --read-rc           | 读默认配置                                                   |
| -C, --read-rc-from file | 从file读配置                                                 |
| -n, --create-rc         | 新建默认配置                                                 |
| -N, --create-rc-to file | 创建配置到file                                               |
| -h, --help              | 显示帮助信息并退出                                           |
| -V, --version           | 显示版本信息并退出                                           |

  最后我计划采用BCC程序来提取进程的各个内存段的内存使用情况，包括其地址。

  

##### 3. 按进程提取页换入信息 mm/pageio.c

**关于页换入换出**

```
当用户访问一个页时，MMU回去查询一个操作系统维护的页表，看看用户访问的页是否存在于内存当中
如果存在，就直接调用；如果没有，就需要开启中断，执行页错误处理程序，在磁盘中找到相应的数据页，并在内存中找到一块空闲，将其载入进来，然后中断返回，执行用户程序，在去访问内存中用户需要的页。
```

**关于SWAP分区**

在Linux下，SWAP的作用类似Windows系统下的“虚拟内存”。当物理内存不足时，拿出部分硬盘空间当SWAP分区（虚拟成内存）使用，从而解决内存容量不足的情况


​	 

```
SWAP意思是交换，顾名思义，当某进程向OS请求内存发现不足时，OS会把内存中暂时不用的数据交换出去，放在SWAP分区中，这个过程称为SWAP OUT。当某进程又需要这些数据且OS发现还有空闲物理内存时，又会把SWAP分区中的数据交换回物理内存中，这个过程称为SWAP IN。

当然，swap大小是有上限的，一旦swap使用完，操作系统会触发OOM-Killer机制，把消耗内存最多的进程kill掉以释放内存。
```

**关于函数挂载点**

```c
如果采用bcc程序来实现这个功能的话，我们就需要找到这么一个函数点，据我了解，相关的函数在mm/pageio.c下:
//linux-2.6.39
*
 *  linux/mm/page_io.c
 *
 *  Copyright (C) 1991, 1992, 1993, 1994  Linus Torvalds
 *
 *  Swap reorganised 29.12.95, 
 *  Asynchronous swapping added 30.12.95. Stephen Tweedie
 *  Removed race in async swapping. 14.4.1996. Bruno Haible
 *  Add swap of shared pages through the page cache. 20.2.1998. Stephen Tweedie
 *  Always use brw_page, life becomes simpler. 12 May 1998 Eric Biederman
 */

#include <linux/mm.h>
#include <linux/kernel_stat.h>
#include <linux/gfp.h>
#include <linux/pagemap.h>
#include <linux/swap.h>
#include <linux/bio.h>
#include <linux/swapops.h>
#include <linux/writeback.h>
#include <asm/pgtable.h>

static struct bio *get_swap_bio(gfp_t gfp_flags,
				struct page *page, bio_end_io_t end_io)
{
	struct bio *bio;

	bio = bio_alloc(gfp_flags, 1);
	if (bio) {
		bio->bi_sector = map_swap_page(page, &bio->bi_bdev);
		bio->bi_sector <<= PAGE_SHIFT - 9;
		bio->bi_io_vec[0].bv_page = page;
		bio->bi_io_vec[0].bv_len = PAGE_SIZE;
		bio->bi_io_vec[0].bv_offset = 0;
		bio->bi_vcnt = 1;
		bio->bi_idx = 0;
		bio->bi_size = PAGE_SIZE;
		bio->bi_end_io = end_io;
	}
	return bio;
}

static void end_swap_bio_write(struct bio *bio, int err)
{
	const int uptodate = test_bit(BIO_UPTODATE, &bio->bi_flags);
	struct page *page = bio->bi_io_vec[0].bv_page;

	if (!uptodate) {
		SetPageError(page);
		/*
		 * We failed to write the page out to swap-space.
		 * Re-dirty the page in order to avoid it being reclaimed.
		 * Also print a dire warning that things will go BAD (tm)
		 * very quickly.
		 *
		 * Also clear PG_reclaim to avoid rotate_reclaimable_page()
		 */
		set_page_dirty(page);
		printk(KERN_ALERT "Write-error on swap-device (%u:%u:%Lu)\n",
				imajor(bio->bi_bdev->bd_inode),
				iminor(bio->bi_bdev->bd_inode),
				(unsigned long long)bio->bi_sector);
		ClearPageReclaim(page);
	}
	end_page_writeback(page);
	bio_put(bio);
}

void end_swap_bio_read(struct bio *bio, int err)
{
	const int uptodate = test_bit(BIO_UPTODATE, &bio->bi_flags);
	struct page *page = bio->bi_io_vec[0].bv_page;

	if (!uptodate) {
		SetPageError(page);
		ClearPageUptodate(page);
		printk(KERN_ALERT "Read-error on swap-device (%u:%u:%Lu)\n",
				imajor(bio->bi_bdev->bd_inode),
				iminor(bio->bi_bdev->bd_inode),
				(unsigned long long)bio->bi_sector);
	} else {
		SetPageUptodate(page);
	}
	unlock_page(page);
	bio_put(bio);
}

/*
 * We may have stale swap cache pages in memory: notice
 * them here and get rid of the unnecessary final write.
 */
int swap_writepage(struct page *page, struct writeback_control *wbc)
{
	struct bio *bio;
	int ret = 0, rw = WRITE;

	if (try_to_free_swap(page)) {
		unlock_page(page);
		goto out;
	}
	bio = get_swap_bio(GFP_NOIO, page, end_swap_bio_write);
	if (bio == NULL) {
		set_page_dirty(page);
		unlock_page(page);
		ret = -ENOMEM;
		goto out;
	}
	if (wbc->sync_mode == WB_SYNC_ALL)
		rw |= REQ_SYNC;
	count_vm_event(PSWPOUT);
	set_page_writeback(page);
	unlock_page(page);
	submit_bio(rw, bio);
out:
	return ret;
}

int swap_readpage(struct page *page)
{
	struct bio *bio;
	int ret = 0;

	VM_BUG_ON(!PageLocked(page));
	VM_BUG_ON(PageUptodate(page));
	bio = get_swap_bio(GFP_KERNEL, page, end_swap_bio_read);
	if (bio == NULL) {
		unlock_page(page);
		ret = -ENOMEM;
		goto out;
	}
	count_vm_event(PSWPIN);
	submit_bio(READ, bio);
out:
	return ret;
}
```

​	

​	 

​	

##### 4. 按进程提取巨页缺页错误相关信息 mm\hugetlb.c\hugetlb_fault

**关于巨页**

```
顾名思义巨页就是比一般常规页面大很多的页面大小。巨页有助于 Linux 系统进行虚拟内存管理。除了标准的 4KB 大小的页面外，它们还能帮助管理内存中的巨大的页面。使用“巨页”，你最大可以定义 1GB 的页面大小。

在系统启动期间，你能用巨页为应用程序预留一部分内存。这部分内存，即被巨页占用的这些存储器永远不会被交换出内存。它会一直保留其中，除非你修改了配置。这会极大地提高像 Oracle 数据库这样的需要海量内存的应用程序的性能。
```

**关于为什么使用巨页**

```
在虚拟内存管理中，内核维护一个将虚拟内存地址映射到物理地址的表，对于每个页面操作，内核都需要加载相关的映射。如果你的内存页很小，那么你需要加载的页就会很多，导致内核会加载更多的映射表。而这会降低性能。

使用“大内存页”，意味着所需要的页变少了。从而大大减少由内核加载的映射表的数量。这提高了内核级别的性能最终有利于应用程序的性能。

简而言之，通过启用“大内存页”，系统具只需要处理较少的页面映射表，从而减少访问/维护它们的开销！
```

我们可以在Linux查看巨页的详细内容：



默认每个页的大小为 2MB（`Hugepagesize`），并且系统中目前有 `0`个“大内存页”（`HugePages_Total`）

这里“巨页”的大小可以从 `2MB` 增加到 `1GB`。

**关于缺页**

用户空间的缺页异常可以分为两种情况：

1.触发异常的线性地址处于用户空间的vma中，但还未分配物理页，如果访问权限OK的话内核就给进程分配相应的物理页了

2.触发异常的线性地址不处于用户空间的vma中，这种情况得判断是不是因为用户进程的栈空间消耗完而触发的缺页异常，如果是的话则在用户空间对栈区域进行扩展，并且分配相应的物理页，如果不是则作为一次非法地址访问来处理，内核将终结进程





**关于巨页缺页**

在进程访问到尚未建立虚实映射的大页面内存区域时，就会产生缺页中断，缺页中断的处理函数do_page_fault()函数。

从do_page_fault()到函数handle_mm_fault()是缺页中断处理的公共流程，不是我们关注的重点，在此不作介绍。

在函数handle_mm_fault()中首先会检查产生缺页中断的内存区域是否是大页面区域，即VM_HUGETLB标志是否设置

如果是，则调用函数hugetlb_fault()进行大页面的映射，这是大页面缺页中断处理的入口函数



所以我们的监测函数点应该设在hugetlb_fault()附近，如果我们需要查到更多的信息的话，可以从hugetlb_fault()的参数中抓取更多的信息。

```c
int hugetlb_fault ( struct mm_struct *mm, 
				    struct vm_area_struct *vma,
				    unsigned long address, 
				    unsigned int flags)
```



#### 2.参数调优

1. **watermark_scale_factor参数**

	其默认值为10，对应内存占比0.1%(10/10000)，可通过"/proc/ sys/vm/watermark_scale_factor"设置，最大为1000。当它的值被设定为1000时，意味着"low"与"min"之间的差值，以及"high"与"low"之间的差值都将是内存大小的10%(1000/10000)。

	watermark_scale_factor的效果就是按照比例来计算water matermark的数值，而非是按照大小来计算。在总内存比较大的情况下，依然可以计算出来比较有效的内存watermark数值。

	```
	场景一，存储类型服务使用page cache使用较多带来的延迟问题
	```

	在对象存储或者其他的存储场景下，会使用大量的page cache。而Linux使用内存的策略比较贪婪，尽量分配，不够了再回收。而默认的回收的阈值比较低，总内存较大的场景下，buddy system中进程缺少较大块的连续内存。而在特定的场景下，例如TCP发送数据的过程中，会有特定的逻辑需要使用大块连续内存，那么就需要先回收内存做compaction，再继续发送数据，从而造成了延迟抖动。

	那么，我们可以调整watermark_scale_factor到较大的数值，内核因此会回收内存较为激进，维持较高的free memory，业务的延迟会相应的变好。同时，思考另外一个问题，就是较高的watermark_scale_factor值会导致available memory较少，更加容易发生OOM。

	```
	场景二，在线/离线混合部署场景下的干扰问题
	```

	通常情况下，在线业务会处理网络请求进行应答，磁盘IO基本就是少量的log，产生不多的page cache。
	而离线业务会从远端获取数据进行计算，保存临时结果，再把最终结果返回给中心节点。那么就会产生很多的page cache。
	大量的page cache会消耗掉free memory，会让在线业务的内存申请陷入到慢路径中而影响了在线业务的延迟。为此，也需要适当的调整watermark_scale_factor，思路还是让内核保持较高的内存水线，但是也需要避免OOM。

​	

2. **min_free_kbytes参数**

	代表系统所保留空闲内存的最低限,主要用途是计算影响内存回收的三个参数.

	min_free_kbytes设的过大:

	```
	这意味着会较早的启动kswapd进行回收，且会回收上来较多的内存，这会使得系统预留过多的空闲内存，从而在一定程度上降低了应用程序可使用的内存量。极端情况下设置min_free_kbytes接近内存大小时，留给应用程序的内存就会太少而可能会频繁地导致OOM的发生。
	
	```

	min_free_kbytes设的过小:

	```
	内存管理区中预留的内存就越少，这样导致系统有一些高优先级分配行为的进程(或内核路径)在特别紧急情况下分配内存失败。若访问预留内存也失败， 那么可能会导致系统进入死锁状态，如kswapd内核线程等通过设置PF_MEMALLOC标志位告诉页面分配器，它们在紧急情况下访问少量的系统预留内存以保证程序的正确运行。
	
	```



3. **swappiness参数**

	默认值为60。用于控制kswapd 内核线程把页面写入交换分区的活跃程度，该值可以设置为0~100。

	该值越小:

	```
	说明写入交换分区的活跃度越低，这样有助于提高系统的I/O性能;
	
	```

	该值越大：

	```
	说明越来越多进程的匿名页面被写入交换分区了，这样有利于系统腾出内存空间，但是发生磁盘交换会导致大量的I/O,影响系统的用户体验和系统性能。
	
	```

	0表示不写入匿名页面到磁盘，直到系统的空闲页面加上文件映射页面的总数少于内存管理区的高水位才启动匿名页面回 收并将其写入交换磁盘。

#### 3.机器学习模型

在实时监控系统中会采集到大量的数据，有些数据具有周期性等时间特征，也称之为时间序列。如果能挖掘出时间序列中所蕴含的信息，实现辅助人工决策，甚至是自动决策，都会为运维工作带来事半功倍的效果。比如分析出内存各项指标的历史规律后，设置动态阈值，得到更加准确的异常报警，减少漏报误报情况的发生，提高应急响应效率。

在本系统中，计划采用预测算法来实现相应的机器学习模型：

- 方案一：神经网络模型,比如LSTM模型
- 方案二：逻辑回归模型



## 项目开发时间计划

|     日期      |                      事项                      |
| :-----------: | :--------------------------------------------: |
| 07/01 - 07/15 |      课题的前期准备工作，寻找相关的函数点      |
| 07/15 - 07/31 | 完成内存水位指标及内存段内存用量的指标信息采集 |
| 08/01 - 08/14 |     完成页换入信息及巨页错误的指标信息采集     |
|     08/15     |                查看中期考核结果                |
| 08/15 - 08/31 |           搭建相应的机器学习预测模型           |
| 09/01 - 09/15 |        在拿到系统数据后对内核的参数调优        |
| 09/15 - 09/29 |                 将功能合入LMP                  |
|     09/30     |                    完成项目                    |


